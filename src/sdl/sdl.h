#ifndef TILETTE_MODULE_SDL
#define TILETTE_MODULE_SDL

#include <SDL2/SDL.h>

typedef struct
{
	//	Common
	int baseWidth;
	int baseHeight;
	int scaleFactor;
	int framerate;

	//	Desktop-Specific
	char* windowName;
	int window_pos_x;
	int window_pos_y;

	//	SDL-Specific
	unsigned int sdl_WindowFlags;
	unsigned int sdl_RendererFlags;
} tilette_InitData;

char* ERROR_CODES[] =
{
	"ERROR_NONE",
	"ERROR_SDL_INIT_SDL_INIT_VIDEO", "ERROR_SDL_INIT_IMG_INIT_PNG",
	"ERROR_SDL_CREATE_WINDOW", "ERROR_SDL_CREATE_RENDERER"
};

typedef struct
{
	SDL_Texture* textureMap;
	int textureWidth;
	int textureHeight;
	int texturesPerLine;
} tilette_Texture;

typedef struct
{
	SDL_Window* sdl_window;
	SDL_Renderer* sdl_renderer;
	tilette_Texture* texture;
	int error;
} tilette_Window;

char* tilette_get_error(tilette_Window* window);

tilette_Window* tilette_init(tilette_InitData initData);
static int _sdl_init();
static int _window_init(tilette_Window* window, tilette_InitData init);

#endif
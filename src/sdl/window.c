#include "sdl.h"
#include "../common/structs.h"

#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

char* tilette_get_error(tilette_Window* window)
{
	return ERROR_CODES[window->error];
}

tilette_Window* tilette_init(tilette_InitData initData)
{
	static bool windowExists = false;

	if(windowExists)
		return NULL;

	tilette_Window* window = malloc(sizeof(tilette_Window));

	windowExists = true;

	window->error = _sdl_init();
	if(window->error)
		return window;

	window->error = _window_init(window, initData);
	if(window->error)
		return window;

	return window;
}

static int _sdl_init()
{
	if(SDL_Init(SDL_INIT_VIDEO))
		return 1;
	
	if(SDL_Init(IMG_INIT_PNG))
		return 2;
	
	return 0;
}

static int _window_init(tilette_Window* window, tilette_InitData init)
{
	window->sdl_window = SDL_CreateWindow
	(
		init.windowName,
		init.window_pos_x, init.window_pos_y,
		init.baseWidth * init.scaleFactor, init.baseHeight * init.scaleFactor,
		init.sdl_WindowFlags
	);

	if(window->sdl_window == NULL)
		return 3;

	window->sdl_renderer = SDL_CreateRenderer
	(
		window->sdl_window, -1,
		init.sdl_RendererFlags
	);
	
	if(window->sdl_renderer == NULL)
		return 4;

	return 0;
}

void tilette_close(tilette_Window* window)
{
	SDL_DestroyWindow(window->sdl_window);
	free(window);
}

void tilette_load_textures(tilette_Window* window, tilette_TextureData* textureData)
{
	if(window->texture != NULL)
		free(window->texture);

	int textureCount;
	for(int i = 0; textureData[i].textureWidth != 0 && i < 200; i++)
	{
		textureCount = i;
	}

	window->texture = malloc(sizeof(tilette_Texture) * textureCount);

	for(int i = 0; i < textureCount; i++)
	{
		window->texture[i].textureMap = IMG_LoadTexture
		(
			window->sdl_renderer,
			textureData[i].path
		);
		window->texture[i].textureWidth = textureData[i].textureWidth;
		window->texture[i].textureHeight = textureData[i].textureHeight;
		window->texture[i].texturesPerLine = textureData[i].texturesPerLine;
	}
}
#ifndef TILETTE_STRUCTS
#define TILETTE_STRUCTS

typedef struct
{
	int textureWidth;
	int textureHeight;
	int texturesPerLine;
	char* path;
} tilette_TextureData;

typedef struct
{
	int textureMap;
	int textureIndex;
} tilette_Tile;

typedef struct
{
	int tileWidth;
	int tileHeight;
	tilette_Tile* tile;
} tilette_TileMap;

typedef struct
{
	int textureMap;
	int textureIndex;
	int pos_x;
	int pos_y;
	int pos_z;
} tilette_Sprite;

#endif
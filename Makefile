##### Sacred #####
SRC=./src
OBJ=./obj
BIN=./bin
##################

SDL_SRC=$(SRC)/sdl
SDL_SRCS=$(wildcard $(SDL_SRC)/*.c)

LINUX_CC=gcc
LINUX_CFLAGS=-Wall
LINUX_SRC=$(SDL_SRC)
LINUX_SRCS=$(SDL_SRCS)
LINUX_OBJ=$(OBJ)/linux
LINUX_OBJS=$(patsubst $(LINUX_SRC)/%.c, $(LINUX_OBJ)/%.o, $(LINUX_SRCS))
LINUX_LIBS=$(SDL_LIBS)
LINUX_BIN=$(BIN)/linux/lib/libtilette.a

linux: $(LINUX_BIN)

$(LINUX_BIN): $(LINUX_OBJS)
	ar cr $(LINUX_BIN) $(LINUX_OBJS)

$(LINUX_OBJ)/%.o: $(LINUX_SRC)/%.c
	$(LINUX_CC) -c $^ $(LINUX_CFLAGS) -o $@

clean-linux:
	$(RM) ./$(LINUX_BIN) ./$(LINUX_OBJ)/*.o

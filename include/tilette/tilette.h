#ifndef LIBTILETTE
#define LIBTILETTE

typedef struct tilette_Window tilette_Window;

typedef struct
{
	int textureWidth;
	int textureHeight;
	int texturesPerLine;
	char* path;
} tilette_TextureData;

typedef struct 
{
	//	Common
	int baseWidth;
	int baseHeight;
	int scaleFactor;
	int framerate;

	//	Desktop-Specific
	char* windowName;
	int window_pos_x;
	int window_pos_y;

	//	SDL-Specific
	unsigned int sdl_WindowFlags;
	unsigned int sdl_RendererFlags;
} tilette_InitData;

char* tilette_get_error(tilette_Window* window);
tilette_Window* tilette_init(tilette_InitData initData);
void tilette_close(tilette_Window* window);

void tilette_load_textures(tilette_Window* window, tilette_TextureData* textureData);

#endif